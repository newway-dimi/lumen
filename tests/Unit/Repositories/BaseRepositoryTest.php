<?php

namespace App\Repositories;

use \Mockery AS M;
use PHPUnit\Framework\TestCase;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class BaseRepositoryTest extends TestCase
{
	private $model;

	public function setUp()
	{
		$this->model = $this->getMockForAbstractClass(Model::class);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::__construct
	 */
	public function testCreateAbstractBaseRepository()
	{
		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$this->model]);

		$this->assertInstanceOf(BaseRepository::class, $baseRepository);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::get
	 */
	public function testGetAbstractBaseRepository()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$get = $baseRepository->get();

		$this->assertInstanceOf(Collection::class, $get);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::with
	 */
	public function testWith()
	{
		$builder = M::spy(Builder::class);

		$userMock = M::mock(Model::class)
			->shouldReceive('with')
			->andReturn($builder)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$with = $baseRepository->with(['tabela']);

		$this->assertInstanceOf(Builder::class, $with);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::first
	 */
	public function testFirstAbstractBaseRepositoryAndReturnNull()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('first')
			->andReturn(null)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$first = $baseRepository->first();

		$this->assertNull($first);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::first
	 */
	public function testFirstAbstractBaseRepository()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('first')
			->andReturn($this->model)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$first = $baseRepository->first();

		$this->assertInstanceOf(Model::class, $first);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::where
	 */
	public function testWhereAbstractBaseRepositoryAndSendSimpleArray()
	{
		$whereCondition = [
			'teste' => 'teste'
		];

		$userMock = M::mock(Model::class)
			->shouldReceive('where')
			->with('teste', '=', 'teste')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$model = $baseRepository->where($whereCondition);

		$this->assertInstanceOf(BaseRepository::class, $model);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::where
	 */
	public function testWhereAbstractBaseRepositoryWithConditionWhere()
	{
		$whereCondition = [
			'teste' => [
				'<>',
				'teste'
			]
		];

		$userMock = M::mock(Model::class)
			->shouldReceive('where')
			->with('teste', '<>', 'teste')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$model = $baseRepository->where($whereCondition);

		$this->assertInstanceOf(BaseRepository::class, $model);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::save
	 */
	public function testSaveAbstractBaseRepository()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('save')
			->andReturn($this->model)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$save = $baseRepository->save($userMock);

		$this->assertInstanceOf(Model::class, $save);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::find
	 */
	public function testFindAbstractBaseRepository()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('find')
			->andReturn($this->model)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$save = $baseRepository->find(1);

		$this->assertInstanceOf(Model::class, $save);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::whereIn
	 */
	public function testWhereInAbstractBaseRepository()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('whereIn')
			->andReturn($this->model)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$whereIn = $baseRepository->whereIn('id', [1]);

		$this->assertInstanceOf(BaseRepository::class, $whereIn);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::whereRaw
	 */
	public function testWhereRawInAbstractBaseRepository()
	{
		$whereCondition = 'campo > campo2';

		$userMock = M::mock(Model::class)
			->shouldReceive('whereRaw')
			->with('campo > campo2')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$model = $baseRepository->whereRaw($whereCondition);

		$this->assertInstanceOf(BaseRepository::class, $model);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::doesntHave
	 */
	public function testDoesntHave()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('doesntHave')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$doesntHave = $baseRepository->doesntHave('relationship');

		$this->assertInstanceOf(BaseRepository::class, $doesntHave);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::withCount
	 */
	public function testWithCount()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('withCount')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$withCount = $baseRepository->withCount(['relationship']);

		$this->assertInstanceOf(BaseRepository::class, $withCount);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::delete
	 */
	public function testDelete()
	{
		$userMock = M::mock(Model::class)
			->shouldReceive('delete')
			->andReturnSelf()
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$userMock]);

		$delete = $baseRepository->delete($userMock);

		$this->assertTrue($delete);
	}

    public function tearDown()
    {
        M::close();
    }
}
