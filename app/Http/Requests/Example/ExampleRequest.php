<?php

namespace App\Http\Requests\Example;

use App\Http\Requests\FormRequest;

class ExampleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        //
    }

    public function messages(): array
    {
        //
    }
}
