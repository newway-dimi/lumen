<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Http\Redirector;
use Illuminate\Container\Container;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidatesWhenResolvedTrait;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class FormRequest extends Request implements ValidatesWhenResolved
{
    use ValidatesWhenResolvedTrait;

    protected $container;
    protected $redirector;
    protected $redirectRoute;
    protected $redirectAction;

    protected $errorBag  = 'default';
    protected $dontFlash = ['password', 'password_confirmation'];

    public function __construct(Request $request)
    {
        //
    }

    protected function getValidatorInstance()
    {
        $factory = $this->container->make(ValidationFactory::class);
        if (method_exists($this, 'validator')) {
            return $this->container->call([$this, 'validator'], compact('factory'));
        }

        return $factory->make(
            $this->validationData(), $this->container->call([$this, 'rules']), $this->messages(), $this->attributes()
        );
    }

    protected function validationData()
    {
        return $this->all();
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->response(
            $this->formatErrors($validator)
        ));
    }

    protected function passesAuthorization()
    {
        if (method_exists($this, 'authorize')) {
            return $this->container->call([$this, 'authorize']);
        }
        return false;
    }

    protected function failedAuthorization()
    {
        throw new UnauthorizedException($this->forbiddenResponse());
    }

    public function response(array $errors)
    {
        return new JsonResponse($errors, 422);
    }

    public function forbiddenResponse()
    {
        return new Response('Forbidden', 403);
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->getMessageBag()->toArray();
    }

    public function setRedirector(Redirector $redirector)
    {
        $this->redirector = $redirector;
        return $this;
    }

    public function setContainer(Container $container)
    {
        $this->container = $container;
        return $this;
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
