<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        if (ENV('APP_ENV') == 'debug') {
            dd($exception);
        }

        if ($exception instanceof ValidationException) {
            return response()->json(['message' => $exception->errors()], 422);
        }

        if ($exception instanceof HttpException) {
            return response()->json(['message' => 'Rota não localizada'], 404);
        }

        return response()->json([
            'message' => ($exception->getMessage()) ? : 'Ocorreu um erro não esperado, tente novamente mais tarde'
        ], ($exception->getCode()) ? : 500);
    }
}
