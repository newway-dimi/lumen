<?php

namespace App\Repositories;

use App\Models\ExampleModel;

class ExampleRepositories extends BaseRepository
{
	public function __construct(ExampleModel $model)
	{
		$this->model = $model;
	}
}
