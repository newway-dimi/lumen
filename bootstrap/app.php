<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Criando a aplicação
|--------------------------------------------------------------------------
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Ativação de recursos (Facade/Eloquent)
|--------------------------------------------------------------------------
*/

// $app->withFacades();
// $app->withEloquent();

/*
|--------------------------------------------------------------------------
| Registro de Container Bindings
|--------------------------------------------------------------------------
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Registro de Middleware
|--------------------------------------------------------------------------
*/

// $app->middleware([]);
// $app->routeMiddleware([]);

/*
|--------------------------------------------------------------------------
| Registro de Providers
|--------------------------------------------------------------------------
*/

$app->register(App\Providers\FormRequestServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Inicializa rotas da aplicação
|--------------------------------------------------------------------------
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
    'prefix'    => 'api',
], function ($router) {
    require __DIR__.'/../routes/api.php';
});

return $app;
